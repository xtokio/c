//g++ buscar.cpp -o buscar
#include <iostream>
#include <string>

using namespace std;

//Mensajes
void Mensaje(string Mensaje, string Color = "Rojo")
{
    if(Color.compare("Negro")==0)
        Color = "30m";
    if(Color.compare("Rojo")==0)
        Color = "31m";
    if(Color.compare("Verde")==0)
        Color = "32m";
    if(Color.compare("Amarillo")==0)
        Color = "33m";
    if(Color.compare("Azul")==0)
        Color = "34m";
    if(Color.compare("Magenta")==0)
        Color = "35m";
    if(Color.compare("Cyan")==0)
        Color = "36m";
    if(Color.compare("Blanco")==0)
        Color = "37m";

    cout << "\033[0;"+Color << Mensaje << " \033[0m\n";
}
//Mensajes

//Buscar
int Buscar(string Texto, string busca)
{
    return Texto.find(busca);
}
//Buscar
int main(int argc,char **argv) 
{
    string Texto = "Buscar agujas en un pajar";
    string Busca = "aguja";

    if (Buscar(Texto,Busca) != string::npos) 
        Mensaje(Busca+ " encontrada!", "Verde");
    else
        Mensaje(Busca+ " no encontrada!", "Rojo");

}