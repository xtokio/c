//g++ markv_filtrar.cpp -o markv_filtrar `mysql_config --cflags --libs`
#include <iostream>
#include <sstream>
#include <stdio.h>
#include <map>
#include <string>
#include <string.h>
#include <my_global.h>
#include <mysql.h>

using namespace std;

//Accesos a BD
string Servidor = "localhost";
string Usuario = "root";
string Password = "V88Tig1";
string BD = "alpr";

//Mensajes
void Mensaje(string Mensaje, string Color = "Rojo")
{
    if(Color.compare("Negro")==0)
        Color = "30m";
    if(Color.compare("Rojo")==0)
        Color = "31m";
    if(Color.compare("Verde")==0)
        Color = "32m";
    if(Color.compare("Amarillo")==0)
        Color = "33m";
    if(Color.compare("Azul")==0)
        Color = "34m";
    if(Color.compare("Magenta")==0)
        Color = "35m";
    if(Color.compare("Cyan")==0)
        Color = "36m";
    if(Color.compare("Blanco")==0)
        Color = "37m";

    cout << "\033[0;"+Color << Mensaje << " \033[0m\n";
}
//Mensajes

void finish_with_error(MYSQL *con)
{
  fprintf(stderr, "%s\n", mysql_error(con));
  mysql_close(con);
  exit(1);
}

MYSQL_RES *Regresa_Query(char const *Query)
{
    MYSQL *con = mysql_init(NULL);

    if (con == NULL)
    {
        fprintf(stderr, "mysql_init() fallo\n");
        exit(1);
    }

    if (mysql_real_connect(con, "localhost", "root", "V88Tig1", "alpr", 0, NULL, 0) == NULL)
    {
        finish_with_error(con);
    }
    
    if (mysql_query(con, Query))
    {
        finish_with_error(con);
    }

    MYSQL_RES *result = mysql_store_result(con);
    mysql_close(con);

    if (result == NULL)
    {
        finish_with_error(con);
    }
    return result;
}

void Ejecuta_Query(char const *Query, char const *Servidor, char const *Usuario, char const *Password, char const *BD)
{
    MYSQL *con = mysql_init(NULL);

    if (con == NULL)
    {
        fprintf(stderr, "mysql_init() fallo\n");
        exit(1);
    }

    if (mysql_real_connect(con, Servidor, Usuario, Password, BD, 0, NULL, 0) == NULL)
    {
        finish_with_error(con);
    }
    
    if (mysql_query(con, Query))
    {
        finish_with_error(con);
    }

    mysql_close(con);
}

int Mayor(string a, string b)
{
    int maxcount = 0, currentcount = 0;//Para ver cual de los dos es mayor

    for(int i = 0; i < a.size(); ++i)
    {

        for(int j = 0; j < b.size(); ++j)
        {
            if(a[i+j] == b[j])
            {
                ++currentcount;
            }
            else
            {
                if(currentcount > maxcount)
                {
                    maxcount = currentcount;
                }//end if
                currentcount = 0;
            }//end else

        }//end for

    }//end for

   return ((int)(((float)maxcount/((float)a.size()))*100));
}

int Comparar(string a, string b){
   return(a.size() > b.size() ? Mayor(a,b) : Mayor(b,a));
}

int *Regresa_Pos(MYSQL_RES *result)
{
    MYSQL_FIELD *field;
    int num_fields = mysql_num_fields(result);

    unsigned int iAuto;
    unsigned int Placas;
    unsigned int Fecha;
    char const *field_id = "iAuto";
    char const *field_placas = "Placas";
    char const *field_fecha = "Fecha";

    char *headers[num_fields];
    for(unsigned int i = 0; (field = mysql_fetch_field(result)); i++) 
    {
        headers[i] = field->name;
        if (strcmp(field_id, headers[i]) == 0) 
        {
            iAuto = i;
        }
        if (strcmp(field_placas, headers[i]) == 0) 
        {
            Placas = i;
        }
        if (strcmp(field_fecha, headers[i]) == 0) 
        {
            Fecha = i;
        }
    }

    int *Pos = new int[3];
    Pos[0] = iAuto;
    Pos[1] = Placas;
    Pos[2] = Fecha;

    return Pos;
}

string int_to_str(int len)
{
    stringstream ss;
    ss << len;
    string Regresa = ss.str();

    return Regresa;
}

map<string,string> Procesar(map<string,string> Arreglo, int Porcentaje)
{
  map<string,string> Arreglo1 = Arreglo;  
  map<string,string> Arreglo2 = Arreglo;
  map<string,string> Arreglo_Filtrado;

  int Res = 0;
  string iAuto,Auto,A,A2,A3,B,B2,B3;

  typedef map<string,string>::iterator itArreglo1_type;
  for(itArreglo1_type iterator = Arreglo1.begin(); iterator != Arreglo1.end(); iterator++) 
  {
        
        map<string,string> Arreglo3;

        A = iterator->first;
        B = iterator->second;

        iAuto = A;
        Auto = B;
        
        //Comparo Elemento B contra todo el arreglo 2 y obtengo porcentaje de comparacion
        typedef map<string,string>::iterator itArreglo2_type;
        for(itArreglo2_type iterator2 = Arreglo2.begin(); iterator2 != Arreglo2.end(); iterator2++) 
        {
            A2 = iterator2->first;
            B2 = iterator2->second;

            Res = Comparar(B,B2);
            //cout << A << " :: " << B << " :: " << A2 << " :: " << B2 << " :: "<< Res << " \n";
            
            if(Res > Porcentaje)
            {
                //cout << "Res: "<<Res<<" Porcentaje: "<<Porcentaje<<"\n";
                //Agrego placas que esten dentro del porcentaje
                Arreglo3[A2] = B2;
            }
        }

        //Busco cual es la placa con mayor numero de caracteres
        typedef map<string,string>::iterator itArreglo3_type;
        for(itArreglo3_type iterator3 = Arreglo3.begin(); iterator3 != Arreglo3.end(); iterator3++) 
        {
            A3 = iterator3->first;
            B3 = iterator3->second;

            if(strlen(B.c_str()) < strlen(B3.c_str()))
            {
                iAuto = A3;
                Auto = B3;
            }

            Mensaje("Placa: " +B3+" Length: " + int_to_str(strlen(B3.c_str())) + "\n");
            //cout << "Placa: " <<B3<<" Length: " << strlen(B3.c_str()) << "\n";
        }

        //Elimino todas las placas restantes y solo me quedo con la de mayor caracteres
        typedef map<string,string>::iterator itArreglo3_type;
        for(itArreglo3_type iterator3 = Arreglo3.begin(); iterator3 != Arreglo3.end(); iterator3++) 
        {
            A3 = iterator3->first;
            B3 = iterator3->second;

            if(Auto != B3)
            {
                int ResBorrado = Arreglo2.erase(A3);
                Arreglo_Filtrado.erase(A3);

                Mensaje(" Borrar: "  + int_to_str(ResBorrado) + "::" + A3 + ":" + B3);
                //cout << " Borrar: "<<ResBorrado<< "::"<< A3 << ":"<< B3;
            }
        }
        Arreglo_Filtrado[iAuto] = Auto;
        Mensaje("\n==============================\nMayor: " + Auto + "\n==============================\n");
        //cout << "\n==============================\nMayor: "<< Auto << "\n==============================\n";
  }

  return Arreglo_Filtrado;
}

int main(int argc, char *argv[])
{ 
    if (argc > 1) 
    {

        MYSQL_ROW row;
        map<string,string> Arreglo;
        map<string,string> Resultado;

        //Consulto la BD por Dia
        string Query = string("SELECT iAuto,Placas FROM autos WHERE DATE(Fecha) = '")+argv[1]+"' ORDER BY iAuto";
        MYSQL_RES *result = Regresa_Query((Query).c_str());

        //Regresas posicion de columnas de la consulta
        int *Pos = Regresa_Pos(result);

        //Agrego resultado de consulta a BD al arreglo que voy a procesar
        while ((row = mysql_fetch_row(result))) 
        {  
            cout << row[Pos[0]] <<"::"<<row[Pos[1]]<<"\n";
            Arreglo[row[Pos[0]]] = row[Pos[1]];
        }
        delete [] Pos;
        mysql_free_result(result);

        //Regresa arreglo filtrado
        //Resultado = Procesar(Arreglo,50);
        //Resultado = Procesar(Arreglo,40);
        Resultado = Procesar(Arreglo,40);

        //Recorro arreglo filtrado para insertar en BD
        int i = 0;
        typedef map<string,string>::iterator it_type;
        for(it_type iterator = Resultado.begin(); iterator != Resultado.end(); iterator++) 
        {
            cout << iterator->first << " : " << iterator->second << "\n";
            string iAuto = iterator->first;
            string Query = string("INSERT INTO filtrados(Placas,Fecha,iAuto)SELECT Placas,Fecha,iAuto FROM autos WHERE iAuto = ")+iAuto;
            i++;
            //cout << Query << " \n";
            //Ejecuta_Query((Query).c_str());
        }
        Mensaje("Total Registros: " + int_to_str(i) + "\n");
        //cout << "Total Registros: " << i << "\n";
    }
    else
        Mensaje("Parametro Fecha no proporcionado 'markv_filtrar 2016-01-31'");

  exit(0);
}