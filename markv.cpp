//g++ markv.cpp -o markv `Magick++-config --cppflags --cxxflags --ldflags --libs` `mysql_config --cflags --libs`
#include <Magick++.h>
#include <iostream>
#include <sstream>
#include <time.h>
#include <vector>


#include <stdexcept>
#include <stdio.h>
#include <string>
#include <functional>


#include <map>
#include <string.h>
#include <my_global.h>
#include <mysql.h>


#include <algorithm>  
#include <cctype>
#include <locale>


using namespace std;
using namespace Magick;


//Accesos a BD
string Servidor = "localhost";
string Usuario = "root";
string Password = "V88Tig1";
string BD = "alpr";


string Servidor_Nube = "107.170.200.104";
string Usuario_Nube = "root";
string Password_Nube = "V88Tig1";
string BD_Nube = "alpr";
//Accesos a BD


string Folder = "/var/spool/plates/";
//string Folder = "/scripts/";


//Mensajes
void Mensaje(string Mensaje, string Color = "Rojo")
{
    if(Color.compare("Negro")==0)
        Color = "30m";
    if(Color.compare("Rojo")==0)
        Color = "31m";
    if(Color.compare("Verde")==0)
        Color = "32m";
    if(Color.compare("Amarillo")==0)
        Color = "33m";
    if(Color.compare("Azul")==0)
        Color = "34m";
    if(Color.compare("Magenta")==0)
        Color = "35m";
    if(Color.compare("Cyan")==0)
        Color = "36m";
    if(Color.compare("Blanco")==0)
        Color = "37m";


    cout << "\033[0;"+Color << Mensaje << " \033[0m\n";
}
//Mensajes


//MySQL
void finish_with_error(MYSQL *con)
{
  fprintf(stderr, "%s\n", mysql_error(con));
  mysql_close(con);
  exit(1);
}


MYSQL_RES *Regresa_Query(char const *Query)
{
    MYSQL *con = mysql_init(NULL);


    if (con == NULL)
    {
        fprintf(stderr, "mysql_init() fallo\n");
        exit(1);
    }


    if (mysql_real_connect(con, "localhost", "root", "V88Tig1", "alpr", 0, NULL, 0) == NULL)
    {
        finish_with_error(con);
    }
    
    if (mysql_query(con, Query))
    {
        finish_with_error(con);
    }


    MYSQL_RES *result = mysql_store_result(con);
    mysql_close(con);


    if (result == NULL)
    {
        finish_with_error(con);
    }
    return result;
}


void Ejecuta_Query(char const *Query, char const *Servidor, char const *Usuario, char const *Password, char const *BD)
{
    MYSQL *con = mysql_init(NULL);


    if (con == NULL)
    {
        fprintf(stderr, "mysql_init() fallo\n");
        exit(1);
    }


    if (mysql_real_connect(con, Servidor, Usuario, Password, BD, 0, NULL, 0) == NULL)
    {
        finish_with_error(con);
    }
    
    if (mysql_query(con, Query))
    {
        finish_with_error(con);
    }


    mysql_close(con);
}


int getTotal(string Placas)
{
    int ResTotal = 0;
    string Query = ("SELECT COUNT(iAuto)AS Total FROM autos WHERE Placas = '")+Placas+"' AND DATE(Fecha) = DATE(NOW()) LIMIT 1";
    MYSQL_RES *result = Regresa_Query((Query).c_str());


    int num_fields = mysql_num_fields(result);


    MYSQL_ROW row;
    MYSQL_FIELD *field;
    unsigned int Total = 0;
    char const *field_id = "Total";


    char *headers[num_fields];
    for(unsigned int i = 0; (field = mysql_fetch_field(result)); i++) 
    {
        headers[i] = field->name;
        if (strcmp(field_id, headers[i]) == 0) 
        {
            Total = i;
        }
    }


    while ((row = mysql_fetch_row(result))) 
    {  
            stringstream ss;
            ss << row[Total];
            string Total_s = ss.str();
            ResTotal = atoi((Total_s).c_str());
    }


    mysql_free_result(result);


    return ResTotal;


}


int *Regresa_Pos(MYSQL_RES *result)
{
    MYSQL_FIELD *field;
    int num_fields = mysql_num_fields(result);


    unsigned int iAuto = 0;
    unsigned int Placas = 0;
    unsigned int Fecha = 0;
    char const *field_id = "iAuto";
    char const *field_placas = "Placas";
    char const *field_fecha = "Fecha";


    char *headers[num_fields];
    for(unsigned int i = 0; (field = mysql_fetch_field(result)); i++) 
    {
        headers[i] = field->name;
        if (strcmp(field_id, headers[i]) == 0) 
        {
            iAuto = i;
        }
        if (strcmp(field_placas, headers[i]) == 0) 
        {
            Placas = i;
        }
        if (strcmp(field_fecha, headers[i]) == 0) 
        {
            Fecha = i;
        }
    }


    int *Pos = new int[3];
    Pos[0] = iAuto;
    Pos[1] = Placas;
    Pos[2] = Fecha;


    return Pos;
}


//MySQL


// trim del inicio (in place)
static inline void ltrim(string &s) 
{
    s.erase(s.begin(), find_if(s.begin(), s.end(), not1(ptr_fun<int, int>(isspace))));
}


// trim del final (in place)
static inline void rtrim(string &s) 
{
    s.erase(find_if(s.rbegin(), s.rend(), not1(ptr_fun<int, int>(isspace))).base(), s.end());
}


// trim del inicio y del final (in place)
static inline void trim(string &s) 
{
    ltrim(s);
    rtrim(s);
}


// trim del inicio (copying)
static inline string ltrimmed(string s) 
{
    ltrim(s);
    return s;
}


// trim del final (copying)
static inline string rtrimmed(string s) 
{
    rtrim(s);
    return s;
}


// trim del inicio y del final (copying)
static inline string trimmed(string s) 
{
    trim(s);
    return s;
}


string exec(const char* cmd) 
{
    char buffer[128];
    string result = "";
    FILE* pipe = popen(cmd, "r");
    if (!pipe) throw runtime_error("popen() failed!");
    try 
    {
        while (!feof(pipe)) 
        {
            if (fgets(buffer, 128, pipe) != NULL)
                result += buffer;
        }
    } 
    catch (...) 
    {
        pclose(pipe);
        throw;
    }
    pclose(pipe);
    return result;
}


void split(const string& s, char c, vector<string>& v) 
{
   string::size_type i = 0;
   string::size_type j = s.find(c);


   while (j != string::npos) {
      v.push_back(s.substr(i, j-i));
      i = ++j;
      j = s.find(c, j);


      if (j == string::npos)
         v.push_back(s.substr(i, s.length()));
   }
}


string getDate()
{
    time_t t = time(NULL);
    tm* timePtr = localtime(&t);


    stringstream ss_year;
    ss_year << timePtr->tm_year+1900;
    string Year = ss_year.str();


    stringstream ss_month;
    ss_month << timePtr->tm_mon+1;
    string Month = ss_month.str();


    stringstream ss_day;
    ss_day << timePtr->tm_mday;
    string Day = ss_day.str();


    stringstream ss_hour;
    ss_hour << timePtr->tm_hour;
    string Hour = ss_hour.str();


    stringstream ss_min;
    ss_min << timePtr->tm_min;
    string Min = ss_min.str();


    stringstream ss_sec;
    ss_sec << timePtr->tm_sec;
    string Sec = ss_sec.str();


    string Fecha = Year+Month+Day+"_"+Hour+Min+Sec;


    return Fecha;
}


string getUID()
{
    //Genero nombre con numero de segundos
    time_t Segundos = time(NULL);
    stringstream ss;
    ss << Segundos;
    string Archivo_UID = ss.str();


    return Archivo_UID;
}


//Filtrar Placas repetidas
int Mayor(string a, string b)
{
    int maxcount = 0, currentcount = 0;//Para ver cual de los dos es mayor


    for(int i = 0; (unsigned)i < a.size(); ++i)
    {


        for(int j = 0; (unsigned)j < b.size(); ++j)
        {
            if(a[i+j] == b[j])
            {
                ++currentcount;
            }
            else
            {
                if(currentcount > maxcount)
                {
                    maxcount = currentcount;
                }//end if
                currentcount = 0;
            }//end else


        }//end for


    }//end for


   return ((int)(((float)maxcount/((float)a.size()))*100));
}


int Comparar(string a, string b)
{
   return(a.size() > b.size() ? Mayor(a,b) : Mayor(b,a));
}


map<string,string> getArreglo(string Bomba)
{
    MYSQL_ROW row;
    map<string,string> Arreglo;


    //Consulto la BD por Dia
    //string Query = string("SELECT iAuto,Placas FROM autos WHERE DATE(Fecha) = '")+argv[1]+"' ORDER BY iAuto";
    //string Query = "select iAuto,Placas from autos where DATE(Fecha) = DATE(NOW()) AND HOUR(Fecha) = HOUR(NOW()) AND MINUTE(Fecha) < 37 ORDER BY iAuto DESC LIMIT 10";
    string Query = string("select iAuto,Placas from autos where DATE(Fecha) = DATE(NOW()) AND Bomba = '")+Bomba+"' ORDER BY iAuto DESC LIMIT 30";
    
    MYSQL_RES *result = Regresa_Query((Query).c_str());


    //Regresas posicion de columnas de la consulta
    int *Pos = Regresa_Pos(result);


    //Agrego resultado de consulta a BD al arreglo que voy a procesar
    while ((row = mysql_fetch_row(result))) 
    {  
        //cout << row[Pos[0]] <<"::"<<row[Pos[1]]<<"\n";
        Arreglo[row[Pos[0]]] = row[Pos[1]];
    }
    delete [] Pos;
    mysql_free_result(result);


    return Arreglo;
}


string Filtrar(map<string,string> Arreglo, string Placas)
{
    string Regresa = "0";
    int Porcentaje = 0;
    string iAuto,Auto,A,B;
    map<string,string> Arreglo_Filtradas;


    //Recorro arreglo buscando placas parecidas
    typedef map<string,string>::iterator itArreglo_type;
    for(itArreglo_type iterator = Arreglo.begin(); iterator != Arreglo.end(); iterator++) 
    {
        iAuto = iterator->first;
        Auto = iterator->second;


        Porcentaje = Comparar(Placas,Auto);
        stringstream P;
        P << Porcentaje;
         
        if(Porcentaje > 40)
        {
            //Agrego todas las placas parecidas
            Arreglo_Filtradas[iAuto] = Auto;
        }
    }


    iAuto = "";
    Auto = "";
    Porcentaje = 0;


    //Recorro arreglo buscando la placa de mayor longitud
    typedef map<string,string>::iterator itArreglo_Filtradas_type;
    for(itArreglo_Filtradas_type iterator_filtradas = Arreglo_Filtradas.begin(); iterator_filtradas != Arreglo_Filtradas.end(); iterator_filtradas++) 
    {
        A = iterator_filtradas->first;
        B = iterator_filtradas->second;


        if((unsigned)Porcentaje < strlen(B.c_str()))
        {
            iAuto = A;
            Auto = B;
            Porcentaje = strlen(B.c_str());
        }
    }


    //Borro placas parecidas que no sean la mayor
    typedef map<string,string>::iterator itArreglo_Borrar_type;
    for(itArreglo_Borrar_type iterator_borrar = Arreglo_Filtradas.begin(); iterator_borrar != Arreglo_Filtradas.end(); iterator_borrar++) 
    {
        A = iterator_borrar->first;
        B = iterator_borrar->second;


        if(B.compare(Auto) != 0)
        {
            string Query = string("DELETE FROM autos WHERE iAuto = ")+A;
            //Borro en servidor Local
            Ejecuta_Query((Query).c_str(),(Servidor).c_str(),(Usuario).c_str(),(Password).c_str(),(BD).c_str());
            //Borro en servidor Nube
            Ejecuta_Query((Query).c_str(),(Servidor_Nube).c_str(),(Usuario_Nube).c_str(),(Password_Nube).c_str(),(BD_Nube).c_str());
            Mensaje("Placa borrada de BD: iAuto: "+A+" Placa: "+B);
        }
        
    }


    if(strlen(Placas.c_str()) <= strlen(Auto.c_str()))
        Regresa = iAuto;


    return Regresa;
}


//Filtrar Placas repetidas


string getImagen(string Archivo, string Camara, string Bomba)
{
    //Formo comando a ejecutar
    string Comando = string("ffmpeg -rtsp_transport tcp -y -i rtsp://admin:admin@")+Camara+" -r 10 -f image2 -frames 1 "+Archivo+" > /dev/null 2>&1";


    //Me conecto a camara para obtener imagen
    string ArchivoImagen = exec(Comando.c_str());


    return Archivo;
}


int Inicio(string Camara,string Bomba, string Pais, string Plaza, string Sucursal, string Imagen, string Archivo_Date)
{
    int Encontro = 0;
    Image image;
    try 
    {
        //Lee imagen y la convierte a un objeto
        image.read( Imagen );


        //Recorto la imagen con las siguientes medidas (width, height, xOffset, yOffset)
        image.crop( Geometry(1080,640, 500, 100) );


        //Guardo la imagen en un archivo
        string Imagen_Crop = Folder+"live_plate_"+Bomba+"_"+Archivo_Date+"_"+getUID()+".jpg";
        image.write(Imagen_Crop);


        //Ejecuto alpr y obtengo resultado en Res
        string Comando = string("alpr -c ")+ Pais + " " + Imagen_Crop + " -n 1";
        string Res = exec(Comando.c_str());


        //En un vector separo por espacios en blanco el resultado para buscar placa
        vector<string> v;
        split(Res, ' ', v);


        string Placas = "Placa";


        //Valido si encontro placa
        if(v.size() > 7)
        {
            Placas = trimmed(v[7]);


            int Repetidas = getTotal(Placas);


            if(Repetidas == 0)
            {
                //Filtrar Placas
                string Resultado = "0";
                map<string,string> Arreglo;
                Arreglo = getArreglo(Bomba);
                Resultado = Filtrar(Arreglo,Placas);
                //Filtrar Placas


                Mensaje("Resultado: "+Resultado+" de Placa: "+Placas+" en Bomba: "+Bomba+" Pais: "+Pais,"Azul");


                if(Resultado.compare("0") == 0)
                {
                    Encontro = 1;


                    string Query = string("INSERT INTO autos(Placas,Bomba,Pais,Plaza,Sucursal,Imagen,iUsuario)VALUES('")+Placas+"','"+Bomba+"','"+Pais+"','"+Plaza+"','"+Sucursal+"','"+Imagen+"',1)";


                    //Inserto en servidor Local
                    Ejecuta_Query((Query).c_str(),(Servidor).c_str(),(Usuario).c_str(),(Password).c_str(),(BD).c_str());
                    //Inserto en servidor Nube
                    Ejecuta_Query((Query).c_str(),(Servidor_Nube).c_str(),(Usuario_Nube).c_str(),(Password_Nube).c_str(),(BD_Nube).c_str());
                    
                    Mensaje("Placa Insertada: "+Placas + " en Bomba: "+Bomba+" Pais: "+Pais,"Verde");
                }
                else
                {
                    Mensaje("Placa repetida en Bomba:"+Bomba+" iAuto: "+Resultado+" Placa: "+Placas+" Pais: "+Pais);
                }
            }
            else
            {
                Mensaje("Placa : "+Placas+" duplicada en COUNT() BD en Bomba: "+Bomba+" Pais: "+Pais);
            }
            
        }
        else
        {
            //Elimino archivos
            remove((Imagen_Crop).c_str());
            Mensaje("No se detecto Placa en Bomba: "+Bomba+" Pais: "+Pais,"Amarillo");
        }
            
    }
    catch( Exception &error_ )
    {
        cout << "Caught exception: " << error_.what() << endl;
        //return 1;
    }


    return Encontro;
}


int main(int argc,char **argv)
{
    //Inicializo libreria Imagemagick
    InitializeMagick(*argv);


    string Camara = "";
    string Bomba = "";
    string Archivo = "";
    string Archivo_Date = getDate();
    string Imagen = "";
    int Encontro;
    
    //Inicio proceso Camara 1
    Mensaje("\n====================== Camara 1 ======================","Azul");
    Encontro = 0;
    Camara = "172.17.32.240";
    Bomba = "b1";
    Archivo = Folder+"live_plate_"+Bomba+"_"+Archivo_Date+".jpg";


    //Obtengo la ruta de la imagen desde la camara
    Imagen = getImagen(Archivo,Camara,Bomba);


    Encontro += Inicio(Camara,Bomba,"us","Tijuana","El Goyito",Imagen,Archivo_Date);
    Encontro += Inicio(Camara,Bomba,"mx","Tijuana","El Goyito",Imagen,Archivo_Date);
    Encontro += Inicio(Camara,Bomba,"au","Tijuana","El Goyito",Imagen,Archivo_Date);


    if (Encontro == 0)
        remove((Imagen).c_str());


    //Inicio proceso Camara 2
    Mensaje("\n====================== Camara 2 ======================","Azul");
    Encontro = 0;
    Camara = "172.17.32.237";
    Bomba = "b2";
    Archivo = Folder+"live_plate_"+Bomba+"_"+Archivo_Date+".jpg";


    //Obtengo la ruta de la imagen desde la camara
    Imagen = getImagen(Archivo,Camara,Bomba);


    Encontro += Inicio(Camara,Bomba,"us","Tijuana","El Goyito",Imagen,Archivo_Date);
    Encontro += Inicio(Camara,Bomba,"mx","Tijuana","El Goyito",Imagen,Archivo_Date);
    Encontro += Inicio(Camara,Bomba,"au","Tijuana","El Goyito",Imagen,Archivo_Date);


    if (Encontro == 0)
        remove((Imagen).c_str());


    //Inicio proceso Camara 3
    Mensaje("\n====================== Camara 3 ======================","Azul");
    Encontro = 0;
    Camara = "172.17.32.234";
    Bomba = "b3";
    Archivo = Folder+"live_plate_"+Bomba+"_"+Archivo_Date+".jpg";


    //Obtengo la ruta de la imagen desde la camara
    Imagen = getImagen(Archivo,Camara,Bomba);


    Encontro += Inicio(Camara,Bomba,"us","Tijuana","El Goyito",Imagen,Archivo_Date);
    Encontro += Inicio(Camara,Bomba,"mx","Tijuana","El Goyito",Imagen,Archivo_Date);
    Encontro += Inicio(Camara,Bomba,"au","Tijuana","El Goyito",Imagen,Archivo_Date);


    if (Encontro == 0)
        remove((Imagen).c_str());


    //Inicio proceso Camara 4
    Mensaje("\n====================== Camara 4 ======================","Azul");
    Encontro = 0;
    Camara = "172.17.32.236";
    Bomba = "b4";
    Archivo = Folder+"live_plate_"+Bomba+"_"+Archivo_Date+".jpg";


    //Obtengo la ruta de la imagen desde la camara
    Imagen = getImagen(Archivo,Camara,Bomba);


    Encontro += Inicio(Camara,Bomba,"us","Tijuana","El Goyito",Imagen,Archivo_Date);
    Encontro += Inicio(Camara,Bomba,"mx","Tijuana","El Goyito",Imagen,Archivo_Date);
    Encontro += Inicio(Camara,Bomba,"au","Tijuana","El Goyito",Imagen,Archivo_Date);


    if (Encontro == 0)
        remove((Imagen).c_str());



    //Inicio proceso Camara 5 nueva 
    Mensaje("\n====================== Camara 5 ======================","Azul");
    Encontro = 0;
    Camara = "172.17.32.190";
    Bomba = "b5";
    Archivo = Folder+"live_plate_"+Bomba+"_"+Archivo_Date+".jpg";


    //Obtengo la ruta de la imagen desde la camara
    Imagen = getImagen(Archivo,Camara,Bomba);


    Encontro += Inicio(Camara,Bomba,"us","Tijuana","El Goyito",Imagen,Archivo_Date);
    Encontro += Inicio(Camara,Bomba,"mx","Tijuana","El Goyito",Imagen,Archivo_Date);
    Encontro += Inicio(Camara,Bomba,"au","Tijuana","El Goyito",Imagen,Archivo_Date);


    if (Encontro == 0)
        remove((Imagen).c_str());
    
    return 0;
}