//g++ cli.cpp -o cli
#include <stdexcept>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

using namespace std;

string exec(const char* cmd) 
{
    char buffer[128];
    string result = "";
    FILE* pipe = popen(cmd, "r");
    if (!pipe) throw runtime_error("popen() failed!");
    try 
    {
        while (!feof(pipe)) 
        {
            if (fgets(buffer, 128, pipe) != NULL)
                result += buffer;
        }
    } 
    catch (...) 
    {
        pclose(pipe);
        throw;
    }
    pclose(pipe);
    return result;
}

int main(int argc,char **argv) 
{
    //Formo comando a ejecutar
    string Comando = string("ls -alsh");

    //Valido si hay parametros
    if (argc > 1) 
    {
        string Parametro = argv[1];
        Comando = Parametro;
    }
    
    //Ejecuto comando
    cout << exec(Comando.c_str());

}