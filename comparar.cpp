#include <iostream>
#include <sstream>
#include <string.h>

using namespace std;

int Mayor(string a, string b)
{
    int maxcount = 0, currentcount = 0;//Para ver cual de los dos es mayor

    for(int i = 0; i < a.size(); ++i)
    {

        for(int j = 0; j < b.size(); ++j)
        {
            if(a[i+j] == b[j])
            {
                ++currentcount;
            }
            else
            {
                if(currentcount > maxcount)
                {
                    maxcount = currentcount;
                }//end if
                currentcount = 0;
            }//end else

        }//end for

    }//end for

   return ((int)(((float)maxcount/((float)a.size()))*100));
}

int Comparar(string a, string b)
{
    cout << a << " :: " << b << "\n";
    return(a.size() > b.size() ? Mayor(a,b) : Mayor(b,a));
}

int main(int argc, char *argv[])
{
    /*
    stringstream ss;
    ss << argv[1];
    string A = ss.str();

    stringstream tt;
    tt << argv[2];
    string B = tt.str();    
    */
    string A = argv[1];
    string B = argv[2];
    
    int Res = Comparar(A,B);
    cout << Res << "\n";
}