//g++ obs_cut.cpp -o obs_cut
#include <iostream>
#include <sstream>

#include <stdexcept>
#include <stdio.h>
#include <string>

using namespace std;

//Mensajes
void Mensaje(string Mensaje, string Color = "Rojo")
{
    if(Color.compare("Negro")==0)
        Color = "30m";
    if(Color.compare("Rojo")==0)
        Color = "31m";
    if(Color.compare("Verde")==0)
        Color = "32m";
    if(Color.compare("Amarillo")==0)
        Color = "33m";
    if(Color.compare("Azul")==0)
        Color = "34m";
    if(Color.compare("Magenta")==0)
        Color = "35m";
    if(Color.compare("Cyan")==0)
        Color = "36m";
    if(Color.compare("Blanco")==0)
        Color = "37m";

    cout << "\033[0;"+Color << Mensaje << " \033[0m\n";
}
//Mensajes

//Ejecuta comando
string exec(const char* cmd) 
{
    char buffer[128];
    string result = "";
    FILE* pipe = popen(cmd, "r");
    if (!pipe) throw runtime_error("popen() failed!");
    try 
    {
        while (!feof(pipe)) 
        {
            if (fgets(buffer, 128, pipe) != NULL)
                result += buffer;
        }
    } 
    catch (...) 
    {
        pclose(pipe);
        throw;
    }
    pclose(pipe);
    return result;
}
//Ejecuta comando

//string to int
int to_int(string Text)
{
    int Number;
    if ( ! (istringstream(Text) >> Number) ) Number = 0;
    return Number;
}
//string to int

//int to string
string to_str(int Number)
{
     ostringstream ss;
     ss << Number;
     return ss.str();
}
//int to string

int main(int argc, char *argv[])
{
    if (argc > 1) 
    {
        string Archivo = argv[1];
        string Comando = "";
        string sDuracion = "";
        
        //Obtengo duracion de video
        Comando = string("ffprobe -i ")+Archivo+" -show_format -v quiet | sed -n 's/duration=//p'";
        sDuracion = exec(Comando.c_str());

        int Duracion = to_int(sDuracion) - 6;
        
        //Ejecuto video cut
        Comando = string("ffmpeg  -async 1 -i ")+Archivo+" -ss 00:00:03 -t 00:00:"+to_str(Duracion)+" -async 1 -strict -2 obs_cut.mp4 > /dev/null 2>&1";
        string Res = exec(Comando.c_str());

        Mensaje("Proceso finalizado","Verde");

    }
    else
        Mensaje("Parametro archivo no proporcionado 'obs_cut video.mp4'");
}