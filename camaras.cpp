//g++ camaras.cpp -o camaras -ljsoncpp
#include <stdexcept>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <jsoncpp/json/json.h> // or jsoncpp/json.h , or json/json.h etc.

using namespace std;

//Mensajes
void Mensaje(string Mensaje, string Color = "Rojo")
{
    if(Color.compare("Negro")==0)
        Color = "30m";
    if(Color.compare("Rojo")==0)
        Color = "31m";
    if(Color.compare("Verde")==0)
        Color = "32m";
    if(Color.compare("Amarillo")==0)
        Color = "33m";
    if(Color.compare("Azul")==0)
        Color = "34m";
    if(Color.compare("Magenta")==0)
        Color = "35m";
    if(Color.compare("Cyan")==0)
        Color = "36m";
    if(Color.compare("Blanco")==0)
        Color = "37m";

    cout << "\033[0;"+Color << Mensaje << " \033[0m\n";
}
//Mensajes

string itos(int x)
{
    stringstream ss;
    ss << x;
    string str = ss.str();
    return str;
}

string exec(const char* cmd) 
{
    char buffer[128];
    string result = "";
    FILE* pipe = popen(cmd, "r");
    if (!pipe) throw runtime_error("popen() failed!");
    try 
    {
        while (!feof(pipe)) 
        {
            if (fgets(buffer, 128, pipe) != NULL)
                result += buffer;
        }
    } 
    catch (...) 
    {
        pclose(pipe);
        throw;
    }
    pclose(pipe);
    return result;
}

string getImagen(string Camara, string Archivo)
{
    //Formo comando a ejecutar
    string Comando = string("ffmpeg -i ")+Camara+" -r 10 -f image2 -frames 1 "+Archivo+" > /dev/null 2>&1";

    //Me conecto a camara para obtener imagen
    exec(Comando.c_str());

    return Archivo;
}

int main(int argc,char **argv) 
{
    ifstream ifs("camaras.json");
    Json::Reader reader;
    Json::Value obj;
    reader.parse(ifs, obj);
    const Json::Value& characters = obj["camaras"];
    for (int i = 0; i < characters.size(); i++)
    {
        Mensaje("\nLink: " + characters[i]["link"].asString(),"Azul");
        Mensaje("\nDescripcion: " + characters[i]["desc"].asString(),"Azul");
        
        getImagen(characters[i]["link"].asString(),"Imagen"+itos(i)+".jpg");

        cout << endl;
    }

}