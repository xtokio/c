//g++ markv_camara_procesar.cpp -o markv_camara_procesar `Magick++-config --cppflags --cxxflags --ldflags --libs` `mysql_config --cflags --libs`
#include <dirent.h>
#include <Magick++.h>
#include <iostream>
#include <sstream>
#include <time.h>
#include <vector>

#include <stdexcept>
#include <stdio.h>
#include <string>
#include <functional>

#include <map>
#include <string.h>
//#include <my_global.h>
//#include <mysql.h>

#include <algorithm>  
#include <cctype>
#include <locale>

#include <my_global.h>
#include <mysql.h>

using namespace std;
using namespace Magick;

//Variables Globales
string Folder_Procesar = "/var/spool/placas/procesar/";
string Folder_Procesadas = "/var/spool/placas/procesadas/";

//Accesos a BD
string Servidor = "localhost";
string Usuario = "root";
string Password = "V88Tig1";
string BD = "alpr";


string Servidor_Nube = "107.170.200.104";
string Usuario_Nube = "root";
string Password_Nube = "V88Tig1";
string BD_Nube = "alpr";

string Servidor_Rendilitros = "107.170.200.104";
string Usuario_Rendilitros = "root";
string Password_Rendilitros = "V88Tig1";
string BD_Rendilitros = "rendilitros";
//Accesos a BD

//Mensajes
void Mensaje(string Mensaje, string Color = "Rojo")
{
    if(Color.compare("Negro")==0)
        Color = "30m";
    if(Color.compare("Rojo")==0)
        Color = "31m";
    if(Color.compare("Verde")==0)
        Color = "32m";
    if(Color.compare("Amarillo")==0)
        Color = "33m";
    if(Color.compare("Azul")==0)
        Color = "34m";
    if(Color.compare("Magenta")==0)
        Color = "35m";
    if(Color.compare("Cyan")==0)
        Color = "36m";
    if(Color.compare("Blanco")==0)
        Color = "37m";


    cout << "\033[0;"+Color << Mensaje << " \033[0m\n";
}
//Mensajes

//MySQL
void finish_with_error(MYSQL *con)
{
  fprintf(stderr, "%s\n", mysql_error(con));
  mysql_close(con);
  exit(1);
}

MYSQL_RES *Regresa_Query(char const *Query)
{
    MYSQL *con = mysql_init(NULL);

    if (con == NULL)
    {
        fprintf(stderr, "mysql_init() fallo\n");
        exit(1);
    }

    if (mysql_real_connect(con, "localhost", "root", "V88Tig1", "alpr", 0, NULL, 0) == NULL)
    {
        finish_with_error(con);
    }
    
    if (mysql_query(con, Query))
    {
        finish_with_error(con);
    }

    MYSQL_RES *result = mysql_store_result(con);
    mysql_close(con);

    if (result == NULL)
    {
        finish_with_error(con);
    }
    return result;
}

void Ejecuta_Query(char const *Query, char const *Servidor, char const *Usuario, char const *Password, char const *BD)
{
    MYSQL *con = mysql_init(NULL);


    if (con == NULL)
    {
        fprintf(stderr, "mysql_init() fallo\n");
        exit(1);
    }

    if (mysql_real_connect(con, Servidor, Usuario, Password, BD, 0, NULL, 0) == NULL)
    {
        finish_with_error(con);
    }
    
    if (mysql_query(con, Query))
    {
        finish_with_error(con);
    }

    mysql_close(con);
}


int getTotal(string Placas)
{
    int ResTotal = 0;
    string Query = ("SELECT COUNT(iAuto)AS Total FROM autos WHERE Placas = '")+Placas+"' AND DATE(Fecha) = DATE(NOW()) LIMIT 1";
    MYSQL_RES *result = Regresa_Query((Query).c_str());

    int num_fields = mysql_num_fields(result);

    MYSQL_ROW row;
    MYSQL_FIELD *field;
    unsigned int Total = 0;
    char const *field_id = "Total";

    char *headers[num_fields];
    for(unsigned int i = 0; (field = mysql_fetch_field(result)); i++) 
    {
        headers[i] = field->name;
        if (strcmp(field_id, headers[i]) == 0) 
        {
            Total = i;
        }
    }

    while ((row = mysql_fetch_row(result))) 
    {  
        stringstream ss;
        ss << row[Total];
        string Total_s = ss.str();
        ResTotal = atoi((Total_s).c_str());
    }

    mysql_free_result(result);
    return ResTotal;

}

int *Regresa_Pos(MYSQL_RES *result)
{
    MYSQL_FIELD *field;
    int num_fields = mysql_num_fields(result);


    unsigned int iAuto = 0;
    unsigned int Placas = 0;
    unsigned int Fecha = 0;
    char const *field_id = "iAuto";
    char const *field_placas = "Placas";
    char const *field_fecha = "Fecha";


    char *headers[num_fields];
    for(unsigned int i = 0; (field = mysql_fetch_field(result)); i++) 
    {
        headers[i] = field->name;
        if (strcmp(field_id, headers[i]) == 0) 
        {
            iAuto = i;
        }
        if (strcmp(field_placas, headers[i]) == 0) 
        {
            Placas = i;
        }
        if (strcmp(field_fecha, headers[i]) == 0) 
        {
            Fecha = i;
        }
    }

    int *Pos = new int[3];
    Pos[0] = iAuto;
    Pos[1] = Placas;
    Pos[2] = Fecha;


    return Pos;
}

//MySQL

void split(const string& s, char c, vector<string>& v) 
{
   string::size_type i = 0;
   string::size_type j = s.find(c);


   while (j != string::npos) {
      v.push_back(s.substr(i, j-i));
      i = ++j;
      j = s.find(c, j);


      if (j == string::npos)
         v.push_back(s.substr(i, s.length()));
   }
}

string split_placa(string Placa)
{
    vector<string> v;
    split(Placa, '/', v);
    return v[5];
}

int filtrar_placa(string Placas)
{
    int Regresa = 0;
    string Buscar = "TIJ";
    string Buscar2 = "TL";
    string Buscar3 = "TJT";
    string Buscar4 = "TJ";
    string Buscar5 = "TI";
    string Buscar6 = "1111";

    if (Placas.find(Buscar) != string::npos || Placas.find(Buscar2) != string::npos || Placas.find(Buscar3) != string::npos  || Placas.find(Buscar4) != string::npos  || Placas.find(Buscar5) != string::npos  || Placas.find(Buscar6) != string::npos)
    {
        Mensaje(  "Placa Filtro encontrada!", "Verde");
        Regresa = 1;
    }
    else
    {
        Regresa = 0;
        Mensaje( "Placa Filtro no encontrada!", "Rojo");
    }
    return Regresa;   
}

string exec(const char* cmd) 
{
    char buffer[128];
    string result = "";
    FILE* pipe = popen(cmd, "r");
    if (!pipe) throw runtime_error("popen() failed!");
    try 
    {
        while (!feof(pipe)) 
        {
            if (fgets(buffer, 128, pipe) != NULL)
                result += buffer;
        }
    } 
    catch (...) 
    {
        pclose(pipe);
        throw;
    }
    pclose(pipe);
    return result;
}

// trim del inicio (in place)
static inline void ltrim(string &s) 
{
    s.erase(s.begin(), find_if(s.begin(), s.end(), not1(ptr_fun<int, int>(isspace))));
}


// trim del final (in place)
static inline void rtrim(string &s) 
{
    s.erase(find_if(s.rbegin(), s.rend(), not1(ptr_fun<int, int>(isspace))).base(), s.end());
}


// trim del inicio y del final (in place)
static inline void trim(string &s) 
{
    ltrim(s);
    rtrim(s);
}


// trim del inicio (copying)
static inline string ltrimmed(string s) 
{
    ltrim(s);
    return s;
}


// trim del final (copying)
static inline string rtrimmed(string s) 
{
    rtrim(s);
    return s;
}


// trim del inicio y del final (copying)
static inline string trimmed(string s) 
{
    trim(s);
    return s;
}

string getDate()
{
    time_t t = time(NULL);
	tm* timePtr = localtime(&t);

    stringstream ss_year;
    ss_year << timePtr->tm_year+1900;
    string Year = ss_year.str();

    stringstream ss_month;
    ss_month << timePtr->tm_mon+1;
    string Month = ss_month.str();
    if(atoi(Month.c_str()) < 10)
        Month = "0"+Month;

    stringstream ss_day;
    ss_day << timePtr->tm_mday;
    string Day = ss_day.str();
    if(atoi(Day.c_str()) < 10)
        Day = "0"+Day;

    stringstream ss_hour;
    ss_hour << timePtr->tm_hour;
    string Hour = ss_hour.str();
    if(atoi(Hour.c_str()) < 10)
        Hour = "0"+Hour;

    stringstream ss_min;
    ss_min << timePtr->tm_min;
    string Min = ss_min.str();
    if(atoi(Min.c_str()) < 10)
        Min = "0"+Min;

    stringstream ss_sec;
    ss_sec << timePtr->tm_sec;
    string Sec = ss_sec.str();
    if(atoi(Sec.c_str()) < 10)
        Sec = "0"+Sec;

    string Fecha = Year+Month+Day+"_"+Hour+Min+Sec;

    return Fecha;
}

vector<string> open(string path = ".") 
{
    DIR*    dir;
    dirent* pdir;
    vector<string> files;
    dir = opendir(path.c_str());

    while (pdir = readdir(dir)) {
        files.push_back(pdir->d_name);
    }
    
    return files;
}

string getUID()
{
    //Genero nombre con numero de segundos
    time_t Segundos = time(NULL);
    stringstream ss;
    ss << Segundos;
    string Archivo_UID = ss.str();

    return Archivo_UID;
}

int Inicio(string Bomba, string Pais, string Plaza, string Sucursal, string Imagen, string Archivo_Date)
{
    int Encontro = 0;
    Image image;
    try 
    {
        //Lee imagen y la convierte a un objeto
        image.read( Imagen );

        //Recorto la imagen con las siguientes medidas (width, height, xOffset, yOffset)
        image.crop( Geometry(1080,640, 500, 100) );

        //Guardo la imagen en un archivo
        string Imagen_Crop = Folder_Procesadas+"placa_"+Bomba+"_"+Archivo_Date+"_"+getUID()+".jpg";
        image.write(Imagen_Crop);

        //Ejecuto alpr y obtengo resultado en Res
        string Comando = string("alpr -c ")+ Pais + " " + Imagen_Crop + " -n 1";
        string Res = exec(Comando.c_str());

        //En un vector separo por espacios en blanco el resultado para buscar placa
        vector<string> v;
        split(Res, ' ', v);

        string Placas = "Placa";
        int Filtro = 0;
        int Total = 0;

        //Valido si encontro placa
        if(v.size() > 7)
        {
            Placas = trimmed(v[7]);

            Filtro = filtrar_placa(Placas);

            if(Filtro == 0)
            {
                Total = getTotal(Placas);

                if(Total == 0)
                {
                    Encontro = 1;
                    string Query = string("INSERT INTO autos(Placas,Bomba,Pais,Plaza,Sucursal,Imagen,iUsuario)VALUES('")+Placas+"','"+Bomba+"','"+Pais+"','"+Plaza+"','"+Sucursal+"','"+split_placa(Imagen)+"',1)";

                    //Inserto en servidor Local
                    Ejecuta_Query((Query).c_str(),(Servidor).c_str(),(Usuario).c_str(),(Password).c_str(),(BD).c_str());
                    //Inserto en servidor Nube
                    Ejecuta_Query((Query).c_str(),(Servidor_Nube).c_str(),(Usuario_Nube).c_str(),(Password_Nube).c_str(),(BD_Nube).c_str());
                    
                    //Inserto en servidor rendilitros
                    Ejecuta_Query((Query).c_str(),(Servidor_Rendilitros).c_str(),(Usuario_Rendilitros).c_str(),(Password_Rendilitros).c_str(),(BD_Rendilitros).c_str());
                    Mensaje("Placa Insertada: "+Placas + " en Bomba: "+Bomba+" Pais: "+Pais,"Verde");

                }
            }
            
        }
        else
        {
            //Elimino archivos
            string Borrar = string("rm ")+(Imagen_Crop).c_str();
            exec(Borrar.c_str());

            Mensaje("No se detecto Placa en Bomba: "+Bomba+" Pais: "+Pais,"Amarillo");
        }
            
    }
    catch( Exception &error_ )
    {
        cout << "Caught exception: " << error_.what() << endl;
        //return 1;
    }

    return Encontro;
}


int main(int argc,char **argv)
{
    //Inicializo libreria Imagemagick
    InitializeMagick(*argv);

    vector<string> f;
    f = open(Folder_Procesar);

    string Bomba = "";
    string Archivo = "";
    string Archivo_Date = "";
    string Imagen = "";
    string Imagen_Procesada = "";
    int Encontro;
    unsigned int i = 0;

    for(i = 0; i < f.size(); i++)
    {
        if(f[i].compare(".") != 0 && f[i].compare("..") != 0 )
        {
            Mensaje(f[i],"Verde");

            //Obtengo la Bomba del archivo
            vector<string> Arreglo;
            split(f[i], '_', Arreglo);
            Bomba = Arreglo[1];
            Archivo_Date = getDate();

            //Proceso Imagen
            Encontro = 0;

            //Obtengo la ruta de la imagen
            Imagen = Folder_Procesar+f[i];
            Imagen_Procesada = Folder_Procesadas+f[i];
            string Comando = string("mv ")+(Imagen).c_str()+" "+(Imagen_Procesada).c_str();
            string Borrar = string("rm ")+(Imagen).c_str();
                    
            Encontro += Inicio(Bomba,"us","Tijuana","El Goyito",Imagen,Archivo_Date);
            Encontro += Inicio(Bomba,"mx","Tijuana","El Goyito",Imagen,Archivo_Date);
            Encontro += Inicio(Bomba,"au","Tijuana","El Goyito",Imagen,Archivo_Date);

            if (Encontro == 0)
                exec(Borrar.c_str());
            else
                exec(Comando.c_str());

        }
    }

    return 0;
}