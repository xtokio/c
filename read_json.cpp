//g++ read_json.cpp -o read_json -ljsoncpp
#include <iostream>
#include <fstream>
#include <jsoncpp/json/json.h> // or jsoncpp/json.h , or json/json.h etc.

using namespace std;

int main() 
{
    ifstream ifs("read_json.json");
    Json::Reader reader;
    Json::Value obj;
    reader.parse(ifs, obj); // reader can also read strings
    cout << "Error: " << obj["error"].asUInt() << endl;
    //cout << "Vehiculos: " << obj["characters"].asString() << endl;
    const Json::Value& characters = obj["vehiculos"]; // array of characters
    for (int i = 0; i < characters.size(); i++){
        cout << "Despacho: " << characters[i]["Despacho"].asUInt();
        cout << "Placas: " << characters[i]["Placas"].asString();
        cout << endl;
    }

}