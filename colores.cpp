#include <iostream>

using namespace std;

//Mensajes
void Mensaje(string Mensaje, string Color = "Rojo")
{
    if(Color.compare("Negro")==0)
        Color = "30m";
    if(Color.compare("Rojo")==0)
        Color = "31m";
    if(Color.compare("Verde")==0)
        Color = "32m";
    if(Color.compare("Amarillo")==0)
        Color = "33m";
    if(Color.compare("Azul")==0)
        Color = "34m";
    if(Color.compare("Magenta")==0)
        Color = "35m";
    if(Color.compare("Cyan")==0)
        Color = "36m";
    if(Color.compare("Blanco")==0)
        Color = "37m";

    cout << "\033[0;"+Color << Mensaje << " \033[0m\n";
}
//Mensajes

int main(int argc, char *argv[])
{
    // \033 es ESC para empezar ESC[0;31m Significa 0 texto normal (1 es bold) y 31m significa rojo y \033[0m para terminar y resetear
    cout << "\033[0;30m Negro text \033[0m\n";
    cout << "\033[0;31m Rojo text \033[0m\n";
    cout << "\033[0;32m Verde text \033[0m\n";
    cout << "\033[0;33m Amarillo text \033[0m\n";
    cout << "\033[0;34m Azul text \033[0m\n";
    cout << "\033[0;35m Magenta text \033[0m\n";
    cout << "\033[0;36m Cyan text \033[0m\n";
    cout << "\033[0;37;44m Blanco text \033[0m\n";

    Mensaje("Rojo: Que pasho mijo?","Rojo");
    Mensaje("Verde: Que pasho mijo?","Verde");
}