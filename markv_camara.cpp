//g++ markv_camara.cpp -o markv_camara
#include <iostream>
#include <sstream>
#include <time.h>

#include <stdexcept>
#include <stdio.h>
#include <string.h>
#include <string>
#include <functional>

#include <algorithm>  
#include <cctype>
#include <locale>

using namespace std;

//Variables Globales
string Folder = "/var/spool/placas/procesar/";

//Mensajes
void Mensaje(string Mensaje, string Color = "Rojo")
{
    if(Color.compare("Negro")==0)
        Color = "30m";
    if(Color.compare("Rojo")==0)
        Color = "31m";
    if(Color.compare("Verde")==0)
        Color = "32m";
    if(Color.compare("Amarillo")==0)
        Color = "33m";
    if(Color.compare("Azul")==0)
        Color = "34m";
    if(Color.compare("Magenta")==0)
        Color = "35m";
    if(Color.compare("Cyan")==0)
        Color = "36m";
    if(Color.compare("Blanco")==0)
        Color = "37m";


    cout << "\033[0;"+Color << Mensaje << " \033[0m\n";
}
//Mensajes

string exec(const char* cmd) 
{
    char buffer[128];
    string result = "";
    FILE* pipe = popen(cmd, "r");
    if (!pipe) throw runtime_error("popen() failed!");
    try 
    {
        while (!feof(pipe)) 
        {
            if (fgets(buffer, 128, pipe) != NULL)
                result += buffer;
        }
    } 
    catch (...) 
    {
        pclose(pipe);
        throw;
    }
    pclose(pipe);
    return result;
}

string getDate()
{
    time_t t = time(NULL);
    tm* timePtr = localtime(&t);

    stringstream ss_year;
    ss_year << timePtr->tm_year+1900;
    string Year = ss_year.str();

    stringstream ss_month;
    ss_month << timePtr->tm_mon+1;
    string Month = ss_month.str();

    stringstream ss_day;
    ss_day << timePtr->tm_mday;
    string Day = ss_day.str();

    stringstream ss_hour;
    ss_hour << timePtr->tm_hour;
    string Hour = ss_hour.str();

    stringstream ss_min;
    ss_min << timePtr->tm_min;
    string Min = ss_min.str();

    stringstream ss_sec;
    ss_sec << timePtr->tm_sec;
    string Sec = ss_sec.str();

    string Fecha = Year+Month+Day+"_"+Hour+Min+Sec;

    return Fecha;
}

string getImagen(string Archivo, string Camara, string Bomba)
{
    //Formo comando a ejecutar
    string Comando = string("ffmpeg -rtsp_transport tcp -y -i rtsp://admin:admin@")+Camara+" -r 10 -f image2 -frames 1 "+Archivo+" > /dev/null 2>&1";

    //Me conecto a camara para obtener imagen
    string ArchivoImagen = exec(Comando.c_str());

    return Archivo;
}

int main(int argc,char **argv)
{
    string Camara = "";
    string Bomba = "";
    string Archivo = "";
    string Archivo_Date = getDate();
    string Imagen = "";

    //Inicio captura Camara 1
    Mensaje("\n====================== Camara 1 ======================","Azul");
    Camara = "172.17.32.240";
    Bomba = "b1";
    Archivo = Folder+"placas_"+Bomba+"_"+Archivo_Date+".jpg";
    //Obtengo la ruta de la imagen desde la camara
    Imagen = getImagen(Archivo,Camara,Bomba);
    Mensaje(Imagen,"Verde");

    //Inicio captura Camara 2
    Mensaje("\n====================== Camara 2 ======================","Azul");
    Camara = "172.17.32.237";
    Bomba = "b2";
    Archivo = Folder+"placas_"+Bomba+"_"+Archivo_Date+".jpg";
    //Obtengo la ruta de la imagen desde la camara
    Imagen = getImagen(Archivo,Camara,Bomba);
    Mensaje(Imagen,"Verde");

    //Inicio captura Camara 3
    Mensaje("\n====================== Camara 3 ======================","Azul");
    Camara = "172.17.32.234";
    Bomba = "b3";
    Archivo = Folder+"placas_"+Bomba+"_"+Archivo_Date+".jpg";
    //Obtengo la ruta de la imagen desde la camara
    Imagen = getImagen(Archivo,Camara,Bomba);
    Mensaje(Imagen,"Verde");

    //Inicio captura Camara 1
    Mensaje("\n====================== Camara 4 ======================","Azul");
    Camara = "172.17.32.236";
    Bomba = "b4";
    Archivo = Folder+"placas_"+Bomba+"_"+Archivo_Date+".jpg";
    //Obtengo la ruta de la imagen desde la camara
    Imagen = getImagen(Archivo,Camara,Bomba);
    Mensaje(Imagen,"Verde");

    //Inicio captura Camara 5
    Mensaje("\n====================== Camara 5 ======================","Azul");
    Camara = "172.17.32.190";
    Bomba = "b5";
    Archivo = Folder+"placas_"+Bomba+"_"+Archivo_Date+".jpg";
    //Obtengo la ruta de la imagen desde la camara
    Imagen = getImagen(Archivo,Camara,Bomba);
    Mensaje(Imagen,"Verde");
   
}