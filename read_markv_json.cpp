//g++ MarkV2Json.cpp -o MarkV2Json -ljsoncpp
#include <iostream>
#include <fstream>
#include <jsoncpp/json/json.h> // or jsoncpp/json.h , or json/json.h etc.

using namespace std;

int main() {
    ifstream ifs("read_markv_json.json");
    Json::Reader reader;
    Json::Value obj;
    reader.parse(ifs, obj); // reader can also read strings
    //cout << "Error: " << obj["error"].asUInt() << endl;
    //cout << "Vehiculos: " << obj["characters"].asString() << endl;
    const Json::Value& characters = obj["mysql"]; // array of characters
    for (int i = 0; i < characters.size(); i++){
        cout << "\nlocal: " << characters[i]["local"].asString();
        cout << "\nnube: " << characters[i]["nube"].asString();
        cout << "\nUsuario: " << characters[i]["usuario"].asString();
        cout << "\nPassword: " << characters[i]["password"].asString();
        cout << endl;
    }

    const Json::Value& characters1 = obj["camaras"]; // array of characters
    for (int i = 0; i < characters1.size(); i++){
        cout << "\nIP: " << characters1[i]["ip"].asString();
        cout << "\nUsuario: " << characters1[i]["usuario"].asString();
        cout << "\nPassword: " << characters1[i]["password"].asString();
        cout << endl;
    }

}