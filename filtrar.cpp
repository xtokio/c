//g++ filtrar.cpp -o filtrar  `mysql_config --cflags --libs`
#include <iostream>
#include <sstream>
#include <map>
#include <string>
#include <string.h>
#include <my_global.h>
#include <mysql.h>

using namespace std;

//Mensajes
void Mensaje(string Mensaje, string Color = "31m")
{
    cout << "\033[0;"+Color << Mensaje << " \033[0m\n";
}
//Mensajes

void finish_with_error(MYSQL *con)
{
  fprintf(stderr, "%s\n", mysql_error(con));
  mysql_close(con);
  exit(1);
}

MYSQL_RES *Regresa_Query(char const *Query)
{
    MYSQL *con = mysql_init(NULL);

    if (con == NULL)
    {
        fprintf(stderr, "mysql_init() fallo\n");
        exit(1);
    }

    if (mysql_real_connect(con, "localhost", "root", "V88Tig1", "alpr", 0, NULL, 0) == NULL)
    {
        finish_with_error(con);
    }
    
    if (mysql_query(con, Query))
    {
        finish_with_error(con);
    }

    MYSQL_RES *result = mysql_store_result(con);
    mysql_close(con);

    if (result == NULL)
    {
        finish_with_error(con);
    }
    return result;
}

void Ejecuta_Query(char const *Query)
{
    MYSQL *con = mysql_init(NULL);

    if (con == NULL)
    {
        fprintf(stderr, "mysql_init() fallo\n");
        exit(1);
    }

    if (mysql_real_connect(con, "localhost", "root", "V88Tig1", "alpr", 0, NULL, 0) == NULL)
    {
        finish_with_error(con);
    }
    
    if (mysql_query(con, Query))
    {
        finish_with_error(con);
    }

    mysql_close(con);
}

int *Regresa_Pos(MYSQL_RES *result)
{
    MYSQL_FIELD *field;
    int num_fields = mysql_num_fields(result);

    unsigned int iAuto = 0;
    unsigned int Placas = 0;
    unsigned int Fecha = 0;
    char const *field_id = "iAuto";
    char const *field_placas = "Placas";
    char const *field_fecha = "Fecha";

    char *headers[num_fields];
    for(unsigned int i = 0; (field = mysql_fetch_field(result)); i++) 
    {
        headers[i] = field->name;
        if (strcmp(field_id, headers[i]) == 0) 
        {
            iAuto = i;
        }
        if (strcmp(field_placas, headers[i]) == 0) 
        {
            Placas = i;
        }
        if (strcmp(field_fecha, headers[i]) == 0) 
        {
            Fecha = i;
        }
    }

    int *Pos = new int[3];
    Pos[0] = iAuto;
    Pos[1] = Placas;
    Pos[2] = Fecha;

    return Pos;
}

//Filtrar Placas repetidas
int Mayor(string a, string b)
{
    int maxcount = 0, currentcount = 0;//Para ver cual de los dos es mayor

    for(int i = 0; i < a.size(); ++i)
    {

        for(int j = 0; j < b.size(); ++j)
        {
            if(a[i+j] == b[j])
            {
                ++currentcount;
            }
            else
            {
                if(currentcount > maxcount)
                {
                    maxcount = currentcount;
                }//end if
                currentcount = 0;
            }//end else

        }//end for

    }//end for

   return ((int)(((float)maxcount/((float)a.size()))*100));
}

int Comparar(string a, string b)
{
   return(a.size() > b.size() ? Mayor(a,b) : Mayor(b,a));
}

map<string,string> getArreglo(string Bomba)
{
    MYSQL_ROW row;
    map<string,string> Arreglo;

    //Consulto la BD por Dia
    //string Query = string("SELECT iAuto,Placas FROM autos WHERE DATE(Fecha) = '")+argv[1]+"' ORDER BY iAuto";
    string Query = string("select iAuto,Placas from autos where DATE(Fecha) = '2016-07-22' AND Bomba = '")+Bomba+"' ORDER BY iAuto DESC LIMIT 30";

    MYSQL_RES *result = Regresa_Query((Query).c_str());

    //Regresas posicion de columnas de la consulta
    int *Pos = Regresa_Pos(result);

    //Agrego resultado de consulta a BD al arreglo que voy a procesar
    while ((row = mysql_fetch_row(result))) 
    {  
        //cout << row[Pos[0]] <<"::"<<row[Pos[1]]<<"\n";
        Arreglo[row[Pos[0]]] = row[Pos[1]];
    }
    delete [] Pos;
    mysql_free_result(result);

    return Arreglo;
}

string Filtrar(map<string,string> Arreglo, string Placas)
{
    string Regresa = "0";
    int Porcentaje = 0;
    string iAuto,Auto,A,B;
    map<string,string> Arreglo_Filtradas;

    //Recorro arreglo buscando placas parecidas
    typedef map<string,string>::iterator itArreglo_type;
    for(itArreglo_type iterator = Arreglo.begin(); iterator != Arreglo.end(); iterator++) 
    {
        iAuto = iterator->first;
        Auto = iterator->second;

        Porcentaje = Comparar(Placas,Auto);
        stringstream P;
        P << Porcentaje;
         
        if(Porcentaje > 40)
        {
            //Agrego todas las placas parecidas
            Arreglo_Filtradas[iAuto] = Auto;
            Mensaje("iAuto: "+iAuto+" Auto: "+Auto+" Porcentaje: "+P.str(),"34m");
        }
        else
            Mensaje("iAuto: "+iAuto+" Auto: "+Auto+" Porcentaje: "+P.str());
    }

    iAuto = "";
    Auto = "";
    Porcentaje = 0;
    
    cout<<"\n========== Filtradas ==========\n";
    
    //Recorro arreglo buscando la placa de mayor longitud
    typedef map<string,string>::iterator itArreglo_Filtradas_type;
    for(itArreglo_Filtradas_type iterator_filtradas = Arreglo_Filtradas.begin(); iterator_filtradas != Arreglo_Filtradas.end(); iterator_filtradas++) 
    {
        A = iterator_filtradas->first;
        B = iterator_filtradas->second;

        Mensaje("iAuto: "+A+" Auto: "+B);

        if(Porcentaje < strlen(B.c_str()))
        {
            iAuto = A;
            Auto = B;
            Porcentaje = strlen(B.c_str());
        }
        
    }

    cout<<"\n========== Borrar Placas ==========\n";
    
    //Borro placas parecidas que no sean la mayor
    typedef map<string,string>::iterator itArreglo_Borrar_type;
    for(itArreglo_Borrar_type iterator_borrar = Arreglo_Filtradas.begin(); iterator_borrar != Arreglo_Filtradas.end(); iterator_borrar++) 
    {
        A = iterator_borrar->first;
        B = iterator_borrar->second;

        if(B.compare(Auto) != 0)
        {
            string Query = string("DELETE FROM autos WHERE iAuto = ")+A;
            Ejecuta_Query((Query).c_str());
            Mensaje((Query).c_str());
        }
        
    }

    cout<<"\n========== Placa Mayor ==========\n";
    Mensaje("iAuto: "+iAuto+" Auto: "+Auto,"32m");

    if(strlen(Placas.c_str()) <= strlen(Auto.c_str()))
        Regresa = iAuto;

    return Regresa;
}

int main(int argc, char *argv[])
{ 
    //Valido si hay parametros para trabajar
    if (argc > 1) 
    {
        string Resultado = "0";
        string Bomba = argv[1];
        string Placas = argv[2];

        //Filtrar Placas
        map<string,string> Arreglo;
        Arreglo = getArreglo(Bomba);
        Resultado = Filtrar(Arreglo,Placas);

        if(Resultado.compare("0")==0)
        {
            Mensaje("\nResultado: "+Resultado+" Inserto Placa: "+Placas);
        }
        else
        {
            Mensaje("\nPlaca encontrada en BD con iAuto: "+Resultado);
        }
        //Filtrar Placas
    }
    else
        Mensaje("Parametros incompletos: Filtrar Bomba Placa (Filtrar b1 xxzzyy)");
    
    exit(0);
}