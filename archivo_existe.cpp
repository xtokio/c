//g++ archivo_existe.cpp -o archivo_existe
#include <iostream>
#include <string>
#include <sys/stat.h>
#include <unistd.h>

using namespace std;

//Mensajes
void Mensaje(string Mensaje, string Color = "Rojo")
{
    if(Color.compare("Negro")==0)
        Color = "30m";
    if(Color.compare("Rojo")==0)
        Color = "31m";
    if(Color.compare("Verde")==0)
        Color = "32m";
    if(Color.compare("Amarillo")==0)
        Color = "33m";
    if(Color.compare("Azul")==0)
        Color = "34m";
    if(Color.compare("Magenta")==0)
        Color = "35m";
    if(Color.compare("Cyan")==0)
        Color = "36m";
    if(Color.compare("Blanco")==0)
        Color = "37m";

    cout << "\033[0;"+Color << Mensaje << " \033[0m\n";
}
//Mensajes

inline bool archivo_existe (const std::string& name) {
  struct stat buffer;   
  return (stat (name.c_str(), &buffer) == 0); 
}

int main(int argc,char **argv) 
{
    string Archivo = "/scripts/date.txt";

    if (archivo_existe(Archivo)) 
        Mensaje(Archivo+ " Si Existe!", "Verde");
    else
        Mensaje(Archivo+ " No Existe!", "Rojo");

}