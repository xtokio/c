#include <algorithm> 
#include <functional> 
#include <cctype>
#include <locale>

#include <iostream>
#include <string>

using namespace std;

// trim from start (in place)
static inline void ltrim(string &s) 
{
    s.erase(s.begin(), find_if(s.begin(), s.end(), not1(ptr_fun<int, int>(isspace))));
}

// trim from end (in place)
static inline void rtrim(string &s) 
{
    s.erase(find_if(s.rbegin(), s.rend(), not1(ptr_fun<int, int>(isspace))).base(), s.end());
}

// trim from both ends (in place)
static inline void trim(string &s) {
    ltrim(s);
    rtrim(s);
}

// trim from start (copying)
static inline string ltrimmed(string s) {
    ltrim(s);
    return s;
}

// trim from end (copying)
static inline string rtrimmed(string s) {
    rtrim(s);
    return s;
}

// trim from both ends (copying)
static inline string trimmed(string s) {
    trim(s);
    return s;
}

int main()
{
    string Palabra = "   Hola ";
    string Tr = trimmed(Palabra);

    cout << Palabra << " Palabra Size: " << Palabra.length() <<"\n";
    cout << Tr << " Tr Size: " << Tr.length()<<"\n";
}