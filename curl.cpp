//g++ curl.cpp -o curl -lcurlpp -lcurl
#include <iostream>
#include <sstream>
#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>

using namespace cURLpp;
using namespace Options;
using namespace std;

int main(int argc,char **argv) {

    stringstream out; 
    stringstream url; 
    url << "127.0.0.1:3000/general/v1/photo/getJson/"; 
    try 
    { 
            curlpp::Cleanup clean; 
            curlpp::Easy request;

            curlpp::options::WriteStream ws(&out); 
            request.setOpt(ws); 

            request.setOpt<curlpp::options::Url>(url.str()); 
            request.setOpt(new curlpp::options::Verbose(true));

            list<string> header; 
            header.push_back("Content-Type: application/x-www-form-urlencoded"); 
            request.setOpt(new curlpp::options::HttpHeader(header));

            string ndid = "ndid=900486100"; 
            request.setOpt(new curlpp::options::PostFields(ndid)); 
            request.setOpt(new curlpp::options::PostFieldSize(ndid.length())); 
            request.perform(); 
    } 
    catch(curlpp::RuntimeError& e) 
    { 
            url.str(""); 
            url << "Error making request of type op : " << e.what(); 
            cout << url; 
            return 0; 
    }
    cout << "===================Response===============\n"; 
    cout << out.str() << endl; 
}