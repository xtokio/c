//g++ crop_image.cpp -o crop_image `Magick++-config --cppflags --cxxflags --ldflags --libs`
#include <Magick++.h> 
#include <iostream>

using namespace std; 
using namespace Magick;

int main(int argc,char **argv)
{
    //Valido si hay parametros
    if (argc > 1) 
    {
        //Inicializo libreria Imagemagick
        InitializeMagick(*argv);

        //Obtengo la ruta de la imagen desde la camara
        string Imagen = argv[1];

        Image image;
        try 
        {
            //Lee imagen y la convierte a un objeto
            image.read( Imagen );

            //Recorto la imagen con las siguientes medidas (width, height, xOffset, yOffset)
            image.crop( Geometry(1080,640, 500, 100) );

            //Guardo la imagen en un archivo
            image.write(Imagen);

            //Regreso ruta de imagen
            cout << Imagen;
        }
        catch( Exception &error_ )
        {
            cout << "Caught exception: " << error_.what() << endl;
            //return 1;
        }
    }
    else
    {
        cout << "Parametro Imagen no encontrado\n";
    }

}