// g++ mysql.cpp -o mysql  `mysql_config --cflags --libs`
#include <iostream>
#include <sstream>
#include <string.h>
#include <my_global.h>
#include <mysql.h>

using namespace std;

void finish_with_error(MYSQL *con)
{
  fprintf(stderr, "%s\n", mysql_error(con));
  mysql_close(con);
  exit(1);
}

MYSQL_RES *Ejecuta_Query(char const *Query)
{
    MYSQL *con = mysql_init(NULL);

    if (con == NULL)
    {
        fprintf(stderr, "mysql_init() fallo\n");
        exit(1);
    }

    if (mysql_real_connect(con, "localhost", "root", "V88Tig1", "autos", 0, NULL, 0) == NULL)
    {
        finish_with_error(con);
    }

    if (mysql_query(con, Query))
    {
        finish_with_error(con);
    }

    MYSQL_RES *result = mysql_store_result(con);
    mysql_close(con);

    if (result == NULL)
    {
        finish_with_error(con);
    }
    return result;
}

int getTotal(string Placas)
{
    unsigned int ResTotal;
    string Query = ("SELECT COUNT(iAutomoviles)AS Total FROM automoviles WHERE Placas LIKE '%")+Placas+"%' AND DATE(Fecha) = DATE(NOW()) LIMIT 1";
    MYSQL_RES *result = Ejecuta_Query((Query).c_str());

    int num_fields = mysql_num_fields(result);

    MYSQL_ROW row;
    MYSQL_FIELD *field;
    unsigned int Total;
    char const *field_id = "Total";

    char *headers[num_fields];
    for(unsigned int i = 0; (field = mysql_fetch_field(result)); i++) 
    {
        headers[i] = field->name;
        if (strcmp(field_id, headers[i]) == 0) 
        {
            Total = i;
        }
    }

    while ((row = mysql_fetch_row(result))) 
    {  
            stringstream ss;
            ss << row[Total];
            string Total_s = ss.str();
            ResTotal = atoi((Total_s).c_str());
    }

    mysql_free_result(result);

    return ResTotal;

}

int main(int argc, char **argv)
{  

    int Total = getTotal("12");
    cout << Total << "\n";
    if (Total > 2)
        cout << "Mayor a 2 \n";
    else
        cout << "Menor a 2 \n";

  MYSQL_RES *result = Ejecuta_Query("SELECT * FROM automoviles ORDER BY iAutomoviles DESC LIMIT 10");

  int num_fields = mysql_num_fields(result);

  MYSQL_ROW row;
  MYSQL_FIELD *field;
  unsigned int iAutomoviles;
  unsigned int Placas;
  unsigned int Fecha;
  char const *field_id = "iAutomoviles";
  char const *field_placas = "Placas";
  char const *field_fecha = "Fecha";

  char *headers[num_fields];
  for(unsigned int i = 0; (field = mysql_fetch_field(result)); i++) 
  {
    headers[i] = field->name;
    if (strcmp(field_id, headers[i]) == 0) 
    {
        iAutomoviles = i;
    }
    if (strcmp(field_placas, headers[i]) == 0) 
    {
        Placas = i;
    }
    if (strcmp(field_fecha, headers[i]) == 0) 
    {
        Fecha = i;
    }
  }

  while ((row = mysql_fetch_row(result))) 
  {  
    cout << "iAutomoviles :" << row[iAutomoviles] << " Placas: " << row[Placas] << " Fecha: " << row[Fecha] << "\n";
  }

  mysql_free_result(result);
  exit(0);
}
